![version boilerplate.0.0.2](https://img.shields.io/badge/version-boilerplate.0.0.2-green.svg?style=flat-square)
![license MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

```
#!bash

NAME
    Makefile -- Create and copy a docker (-sync) configuration

SYNOPSIS
    make docker plain 
    make docker sync formac 
    make docker sync fortoolbox 

DESCRIPTION
    The Makefile automates configuration for docker.

    The following options are available:

    docker plain                Creates docker-compose.yml from docker-compose.v2.yml. 
                                Works on Linux.

    docker sync formac          Creates docker-compose.yml from docker-compose.v2.dockerformac.yml. 
                                Works on MacOS with 'docker for mac' and 'docker-sync'.
                                
    docker sync fortoolbox      Creates docker-compose.yml from docker-compose.v2.dockerformac.yml. 
                                Works on MacOS with 'docker', 'docker toolbox (deprecated)' and 'docker-sync'.

REQUIREMENTS
    Installation of 'docker', 'docker-compose' and 'docker-sync '(optional). 
    'docker-sync' requires 'ruby' and 'fswatch' (see https://github.com/EugenMayer/docker-sync/wiki).

    See also: https://bitbucket.org/account/user/teufels/projects/DOC.

    Using the make command requires configuration of the corresponding 'docker.ini' and an existing 't_installer'. 
    't_installer' has to be cratetd first by using 't_creator'.

    See also: https://bitbucket.org/teufels/t_creator

ERRORCODES:
    TODO: Helpful description of errorcodes and possible solutions.
    [a3ffba251c6974f261b35f4657a9ba17]
        Description:
            Missing argument
        Solution:
            Execute 'make docker <argument>'
            Possible arguments are:
                - plain
                - sync formac 
                - sync toolbox
    [58ea140d644dbf40a41e6a38c21b10fa]
    [01ffa23315f06807ac45c884db8680e9]
    [6ba0ed25afcb12edadd37a54767870fd]
    [34aa56ce9a9b30b3be59fc36d773295b]
    [a011525ae58805e36cc6ece9f8ed3d68]
    [ac86811491a1dccf7f3fb8969dec9fc8]
    [d8de8e7e50183029ebd1d3ebe3e5a030]
    [d12b241f7d83a0558f5620ea6a89acd9]

LISENCE:
    The MIT License (MIT)

    Copyright (c) 2016 
    Andreas Hafner <a.hafner@teufels.com>, 
    Dominik Hilser <d.hilser@teufels.com>, 
    Georg Kathan <g.kathan@teufels.com>, 
    Hendrik Krüger <h.krueger@teufels.com>,
    Perrin Ennen <p.ennen@teufels.com>, 
    Timo Bittner <t.bittner@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    
```
