#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../bin/.config.sh"

if [ "$#" -ne 1 ]; then
    echo "No type defined"
    exit 1
fi

mkdir -p -- "${BACKUP_DIR}"

case "$1" in    
    ###################################
    ## MySQL export from dev database
    ###################################
    "mysqlexport")
        if [ -f "${BACKUP_DIR}/mysql.sql" ]; then
            logMsg "Removing old backup file..."
            rm -f -- "${BACKUP_DIR}/mysql.sql"
        fi

        logMsg "Starting MySQL export..."
        mysqldump --opt --single-transaction --skip-lock-tables --add-drop-table --default-character-set=utf8 dev > "${BACKUP_DIR}/mysql.sql"
        logMsg "Finished"
        ;;    
esac
